Date calculator program written in python

Needs python 2 environment to run, can be installed from
https://www.python.org/downloads/
Should work on python 3 as well 

Test python after installation 
running python from a command line should open a shell with the version string info

To run the program, specify start date and end date with the -s and -e flags like:

On unix/Linux 

chmod +x calc_days.py 

./calc_days.py -s 25/12/1984 -e 04/07/1984

returns 173
 
On Windows

calc_days.py -s 25/12/1984 -e 04/07/1984


Python was chosen because 

It is eazy to install and run the program from the command line
Is present on Linux by default
Is platform independent


    
