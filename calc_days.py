#!/usr/bin/python2
import argparse

# Purpose
# calculate no of days based on start and end date inputs 
# vanilla implementaion no libraries.

days_mon_map = { 1:31, 2:28, 3:31, 4:30, 5:31, 6:30, 7:31, 8:31, 9:30, 10:31, 11:30, 12:31 }

def get_elapsed_years(year_1, year_2):
  return year_2 - year_1
  

def get_elapsed_months(month_1, month_2):
  return month_2 - month_1

def get_elapsed_days(day_1, day_2):
  return day_2 - day_1 


def get_days_offset(d1, m1, d2, m2):
  #take in months and dates and find diff 
  whole_month_days = 0
  days = 0 

  if m1 != m2:
    #days for completing month + days in end month  
    days_offset = (days_mon_map[m1] - d1) + d2  
  
    #find if there are whole months in between
    for i in range(m1+1, m2):
      whole_month_days = whole_month_days + days_mon_map[i]
    days = days_offset + whole_month_days 
  else:
    days = d2 - d1

  return days   

    
def is_leap(year):
  if year % 4 == 0 and year % 100 != 0 or year % 400 == 0:
    return 1
  return 0


def get_leaps(y1, y2):
  count = 0 
  for year in range(y1, y2 + 1):
    if is_leap(year):
      count = count + 1
  return count


#take in years and return diff
def years_to_days(d1, m1, y1, d2, m2, y2):
  whole_year_days = (get_elapsed_years(y1, y2) - 1) * 365    
  days_in_first_year = get_days_offset(d1, m1, days_mon_map[12], 12)
  days_in_last_year = get_days_offset(1, 1, d2, m2)
  total = whole_year_days + days_in_first_year + days_in_last_year
  total = total + get_leaps(y1,y2)
  return total  


#ignore start and end days. consider only full days 
def exclude_days(days):
  days = days - 1 
  return days if days > 0 else 0


#input date component array ret 0 if ok else 1  
def is_valid_date(date):
  ret = 0
  try: 
    day = int(date[0])
    mon = int(date[1])
    year = int(date[2])
    if mon < 1 or mon > 12:
      ret = 1  
    if day < 1 or day  > days_mon_map[mon]:
      ret = 1      
    if year < 1901 or year > 2999:
      ret = 1
  except:
    ret = 1
 
  return ret



def main():


  #extract dates , validate inputs 
  parser = argparse.ArgumentParser()
  parser.add_argument('-s', '--start')
  parser.add_argument('-e', '--end')
  args = parser.parse_args()

  if args.start and args.end:
    start_date = args.start.split('/')
    end_date = args.end.split('/')
    if (is_valid_date(start_date) != 0 or is_valid_date(end_date) != 0):
      print('Dates not valid !')
      quit()
    else:
      start_day = int(start_date[0])
      start_month = int(start_date[1])
      start_year = int(start_date[2])
      end_day = int(end_date[0])
      end_month = int(end_date[1])
      end_year = int(end_date[2])

      #handle reversed start and end date inputs    
      if start_year > end_year:
        start_year, end_year = end_year, start_year
        start_month, end_month = end_month, start_month
        start_day, end_day = end_day, start_day
      elif start_year == end_year and start_month > end_month:
        start_month, end_month = end_month, start_month
        start_day, end_day = end_day, start_day
      elif start_year == end_year and start_month == end_month and start_day > end_day:
        start_day, end_day = end_day, start_day
  else:  
    print('Please specify start date and end date with -s and -e parameters')
    print('Ex: calc_days.py -s 25/12/1984 -e 04/07/1984')
    print('Date input is in dd/mm/yyyy format')
    quit()  


  #Main logic. simple scenario same year and month 
  if ( get_elapsed_years(start_year, end_year) == 0 ):
    #we are in the same year.
    if ( get_elapsed_months(start_month, end_month) == 0 ):
      #we are in the same month. 
      print(exclude_days(abs(get_elapsed_days(start_day, end_day))))
    else:
      #different months    
      print(abs(exclude_days(get_days_offset(start_day, start_month, end_day, end_month))))     
  else:
      #different years 
      print(abs(years_to_days(start_day, start_month, start_year, end_day, end_month, end_year))) 


   

if __name__ == '__main__':
  #entry point
  main()


